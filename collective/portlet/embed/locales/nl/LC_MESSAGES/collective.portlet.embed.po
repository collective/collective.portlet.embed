msgid ""
msgstr ""
"Project-Id-Version: 1.1.dev0\n"
"POT-Creation-Date: 2012-09-04 09:07+0000\n"
"PO-Revision-Date: 2012-09-04 09:48+0100\n"
"Last-Translator: Ralph Jacobs <ralph@fourdigits.nl>\n"
"Language-Team: Four Digits <support@fourdigits.nl>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"Language-Code: nl\n"
"Language-Name: Dutch\n"
"Preferred-Encodings: utf-8 latin1\n"
"Domain: collective.portlet.embed\n"

#: ./portlet.py:24
msgid "HTML Code to embed"
msgstr "HTML Code welke embed moet worden"

#: ./portlet.py:25
msgid "The html snippet you want to use. It can be iframe, javascript, html"
msgstr "De html snippet welke je wilt gebruiken. Dit kan een iframe, javascript of html zijn"

#. Default: "A portlet which can display embed HTML code."
#: ./portlet.py:61
msgid "description_portlet"
msgstr "Een portlet welke embedded HTML code laat zien."

#. Default: "Add embed portlet"
#: ./portlet.py:59
msgid "title_add_portlet"
msgstr "Voeg embed portlet toe"

#. Default: "Edit embed portlet"
#: ./portlet.py:71
msgid "title_edit_portlet"
msgstr "Wijzig embed portlet"

#. Default: "Embed portlet"
#: ./portlet.py:31
msgid "title_portlet"
msgstr "Embed portlet"

